package testmaker;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import org.apache.commons.io.IOUtils;
import org.eclipse.jdt.core.dom.AST;
import org.eclipse.jdt.core.dom.ASTParser;
import org.eclipse.jdt.core.dom.CompilationUnit;

public class DtoTestMaker {
	public static void main(String[] args) throws IOException {
		File classToRead = new File(
				"/home/tifred/git/msgenerator/src/main/java/fr/msgenerator/dto/application/MicroService.java");
		ASTParser astParser = ASTParser.newParser(AST.JLS_Latest);

		final FileReader inputStream = new FileReader(classToRead);
		char[] buffer = IOUtils.toCharArray(inputStream);

		astParser.setSource(buffer);

		DtoVisitor designClassVisitor = new DtoVisitor();
		CompilationUnit compilationUnit = (CompilationUnit) astParser.createAST(null);
		compilationUnit.accept(designClassVisitor);

	}
}
