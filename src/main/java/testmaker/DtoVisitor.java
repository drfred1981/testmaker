package testmaker;

import org.eclipse.jdt.core.dom.ASTVisitor;
import org.eclipse.jdt.core.dom.FieldDeclaration;
import org.eclipse.jdt.core.dom.PackageDeclaration;
import org.eclipse.jdt.core.dom.Type;

public class DtoVisitor extends ASTVisitor {

	/** The package name of the parsed class. */
	private String packageName;

	/** The class name. */
	private String className;

	/** {@inheritDoc} */
	@Override
	public boolean visit(final PackageDeclaration packageDeclaration) {
		packageName = packageDeclaration.getName().toString();

		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jdt.core.dom.ASTVisitor#visit(org.eclipse.jdt.core.dom.
	 * FieldDeclaration)
	 */
	/** {@inheritDoc} */
	@Override
	public boolean visit(final FieldDeclaration node) {
		final Type type = node.getType();

		System.out.println(node.toString());

		return true;
	}

}
